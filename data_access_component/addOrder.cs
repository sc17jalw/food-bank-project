using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public class Order : TableEntity {
        public string foodID {get; set;}
        public string userID {get; set;}
        public Order() {}
        public Order(string foodID, string userID) {
            // Generate orderID
            string orderID = string.Concat(foodID, userID).GetHashCode().ToString();
            RowKey = orderID;
            PartitionKey = orderID;
            this.foodID = foodID;
            this.userID = userID;
        }
    }
    public static class addOrder
    {
        [FunctionName("addOrder")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to add an order to the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string foodID = data.foodID;
            string userID = data.userID;

            // Add order to Azure database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable ordersTable = tableClient.GetTableReference("orders");
            try {
                TableOperation insertOperation = TableOperation.Insert(new Order(foodID, userID));
                TableResult result = await ordersTable.ExecuteAsync(insertOperation);
            } catch (StorageException e) {
                return new OkObjectResult(string.Concat("{\"success\": false, \"message\":\"", e.ToString(), "\"}"));
            }
            
            return new OkObjectResult("{\"success\": true}");
        }
    }
}

