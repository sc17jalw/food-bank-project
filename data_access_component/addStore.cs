using System.Net.Http;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public class Store : TableEntity
    {
        public double latitude {get; set;}
        public double longitude {get; set;}
        public string information {get; set;}
        public string address {get; set;}
        public Store() {}
        public Store(string name, double latitude, double longitude, string information, string address)
        {
            PartitionKey = name;
            RowKey = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.information = information;
            this.address = address;
        }
    }
    public static class addStore
    {
        [FunctionName("addStore")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to add store to the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string name = data.name;
            string postcode = data.postcode;
            string information = data.information;
            string address = data.address;

            // Get longitude and latitude.
            HttpClient client = new HttpClient();
            var postcodeResult = client.GetStringAsync("http://api.postcodes.io/postcodes/" + postcode).Result;
            dynamic postcodeData = JsonConvert.DeserializeObject(postcodeResult);
            double latitude = postcodeData.result.latitude;
            double longitude = postcodeData.result.longitude;

            // Add store to Azure database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable storesTable = tableClient.GetTableReference("stores");
            try {
                TableOperation insertOperation = TableOperation.Insert(new Store(name, latitude, longitude, information, address));
                TableResult result = await storesTable.ExecuteAsync(insertOperation);
            } catch (StorageException e) {
                return new OkObjectResult(string.Concat("{\"success\": false, \"message\":\"", e.ToString(), "\"}"));
            }
            
            return new OkObjectResult("{\"success\": true}");
        }
    }
}
