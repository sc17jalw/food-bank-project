using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public static class getOrdersUser
    {
        [FunctionName("getOrdersUser")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to retrieve orders for a user, from the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string userID = data.userID;
            
            // Execute query.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable ordersTable = tableClient.GetTableReference("orders");
            TableQuery<Order> ordersDateQuery = new TableQuery<Order>().Where(
                TableQuery.GenerateFilterCondition("userID", QueryComparisons.Equal, userID)
            );
            var queryResult = ordersTable.ExecuteQuery(ordersDateQuery);
            
            // Format output.
            OutputOrders outputObject = new OutputOrders();
            foreach (Order order in queryResult) {
                outputObject.success = true;
                outputObject.orders.Add(new OrderOutput(order.RowKey, order.foodID, order.userID, order.Timestamp.DateTime.ToString()));
            }

            return new OkObjectResult(JsonConvert.SerializeObject(outputObject));
        }
    }
}
