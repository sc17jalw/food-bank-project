using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;

namespace FoodBank.DataAccess
{
    public class SearchOutput {
        public string searchID;
        public string category;
        public double latitude;
        public double longitude;
        public double distance;
        
        public SearchOutput(string searchID, string category, double latitude, double longitude, double distance) {
            this.searchID = searchID;
            this.category = category;
            this.latitude = latitude;
            this.longitude = longitude;
            this.distance = distance;
        }
    }

    public class OutputSearches {
        public List<SearchOutput> searches = new List<SearchOutput>();
        public Boolean success = false;
    }
    public static class getSearches
    {
        [FunctionName("getSearches")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to retireve searches in given distance of a location from the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            double latitude = data.latitude;
            double longitude = data.longitude;
            double distance = data.distance;
            
            // Calculate target coordinates.
            Geolocation.CoordinateBoundaries boundaries = new Geolocation.CoordinateBoundaries(new Geolocation.Coordinate(latitude, longitude), distance);
            
            // Execute query.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable searchesTable = tableClient.GetTableReference("searches");
            TableQuery<Search> searchesDistanceQuery = new TableQuery<Search>().Where(
                TableQuery.CombineFilters(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("latitude", QueryComparisons.GreaterThanOrEqual, boundaries.MinLatitude),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("latitude", QueryComparisons.LessThanOrEqual, boundaries.MaxLatitude)
                    ),
                    TableOperators.And,
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("longitude", QueryComparisons.GreaterThanOrEqual, boundaries.MinLongitude),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("longitude", QueryComparisons.LessThanOrEqual, boundaries.MaxLongitude)
                    )
                )
            );
            var queryResult = searchesTable.ExecuteQuery(searchesDistanceQuery);

            // Format output.
            OutputSearches outputObject = new OutputSearches();
            foreach (Search search in queryResult) {
                outputObject.success = true;
                outputObject.searches.Add(new SearchOutput(search.RowKey, search.category, search.latitude, search.longitude, search.distance));
            }

            return new OkObjectResult(JsonConvert.SerializeObject(outputObject));
        }
    }
}

