using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public static class getOrder
    {
        [FunctionName("getOrder")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to retrieve order from database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string orderID = data.orderID;            

            // Retieve Entity from database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable ordersTable = tableClient.GetTableReference("orders");
            string returnString = "";
            try {
                TableOperation retrieveOperation = TableOperation.Retrieve<Order>(orderID, orderID);
                TableResult result = await ordersTable.ExecuteAsync(retrieveOperation);
                Order retrievedOrder = result.Result as Order;
                if (retrievedOrder != null) {
                    returnString = string.Concat(
                        "{\"orderID\":\"", retrievedOrder.RowKey,
                        "\",\"foodID\":\"", retrievedOrder.foodID,
                        "\",\"userID\":\"", retrievedOrder.userID,
                        "\",\"timestamp\":\"", retrievedOrder.Timestamp.DateTime.ToString(), "\"}"
                    );
                } else {
                    returnString = "{\"success\": false, \"message\":\"Entity not found.\"}";
                }
            } catch (StorageException e) {
                returnString = string.Concat("{\"success\": false, \"message\":\"", e.ToString(), "\"}");
            }
            return new OkObjectResult(returnString);
        }
    }
}

