using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public class FoodItem : TableEntity {
        public string name {get; set;}
        public string storeName {get; set;}
        public string category {get; set;}
        public double quantity {get; set;}
        public string unit {get; set;}
        public FoodItem() {}
        public FoodItem(string name, string storeName, string category, double quantity, string unit) {
            // Generate foodID.
            string foodID = string.Concat(DateTime.Now.GetHashCode(), name, storeName).GetHashCode().ToString();
            RowKey = foodID;
            PartitionKey = foodID;
            this.name = name;
            this.storeName = storeName;
            this.category = category;
            this.quantity = quantity;
            this.unit = unit;
        }
    }
    public static class addFoodItem
    {
        [FunctionName("addFoodItem")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to add a food item to the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string name = data.name;
            string storeName = data.store_name;
            string category = data.category;
            double quantity = data.quantity;
            string unit = data.unit;

            // Add food item to Azure database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable foodsTable = tableClient.GetTableReference("foods");
            try {
                TableOperation insertOperation = TableOperation.Insert(new FoodItem(name, storeName, category, quantity, unit));
                TableResult result = await foodsTable.ExecuteAsync(insertOperation);
            } catch (StorageException e) {
                return new OkObjectResult(string.Concat("{\"success\": false, \"message\":\"", e.ToString(), "\"}"));
            }

            return new OkObjectResult("{\"success\": true}");
        }
    }
}
