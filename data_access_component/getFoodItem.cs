using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public static class getFoodItem
    {
        [FunctionName("getFoodItem")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to retrieve food item from database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string foodID = data.foodID;            

            // Retieve Entity from database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable foodsTable = tableClient.GetTableReference("foods");
            string returnString = "";
            try {
                TableOperation retrieveOperation = TableOperation.Retrieve<FoodItem>(foodID, foodID);
                TableResult result = await foodsTable.ExecuteAsync(retrieveOperation);
                FoodItem retrievedFoodItem = result.Result as FoodItem;
                if (retrievedFoodItem != null) {
                    returnString = string.Concat(
                        "{\"foodID\":\"", retrievedFoodItem.RowKey,
                        "\",\"name\":\"", retrievedFoodItem.name,
                        "\",\"store_name\":\"", retrievedFoodItem.storeName,
                        "\",\"category\":\"", retrievedFoodItem.category,
                        "\",\"quantity\":", retrievedFoodItem.quantity.ToString(),
                        ",\"unit\":\"", retrievedFoodItem.unit, "\"}"
                    );
                } else {
                    returnString = "{\"success\": false, \"message\":\"Entity not found.\"}";
                }
            } catch (StorageException e) {
                returnString = string.Concat("{\"success\": false, \"message\":\"", e.ToString(), "\"}");
            }
            return new OkObjectResult(returnString);
        }
    }
}
