# Data Access Component
This component is responsible for the data of the system.

This component has been deployed, and the API can be accessed with the following URL:

[https://dataaccesscomponentfoodbank.azurewebsites.net/api/](https://dataaccesscomponentfoodbank.azurewebsites.net/api/)
