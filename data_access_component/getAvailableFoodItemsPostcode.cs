using System;
using System.Net.Http;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public static class getAvailableFoodItemsPostcode
    {
        [FunctionName("getAvailableFoodItemsPostcode")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to get all available food items, within a distance from a postcode.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string postcode = data.postcode;
            double distance = data.distance;

            // Get longitude and latitude.
            HttpClient client = new HttpClient();
            var postcodeResult = client.GetStringAsync("http://api.postcodes.io/postcodes/" + postcode).Result;
            dynamic postcodeData = JsonConvert.DeserializeObject(postcodeResult);
            double latitude = postcodeData.result.latitude;
            double longitude = postcodeData.result.longitude;

            // Calculate target coordinates.
            Geolocation.CoordinateBoundaries boundaries = new Geolocation.CoordinateBoundaries(new Geolocation.Coordinate(latitude, longitude), distance);
            
            // Execute query to find stores within a given distance.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable storesTable = tableClient.GetTableReference("stores");
            TableQuery<Store> storesDistanceQuery = new TableQuery<Store>().Where(
                TableQuery.CombineFilters(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("latitude", QueryComparisons.GreaterThanOrEqual, boundaries.MinLatitude),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("latitude", QueryComparisons.LessThanOrEqual, boundaries.MaxLatitude)
                    ),
                    TableOperators.And,
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("longitude", QueryComparisons.GreaterThanOrEqual, boundaries.MinLongitude),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("longitude", QueryComparisons.LessThanOrEqual, boundaries.MaxLongitude)
                    )
                )
            );
            var queryResultStores = storesTable.ExecuteQuery(storesDistanceQuery);

            // Execute query to find food items belonging to stores in list.
            CloudTable foodsTable = tableClient.GetTableReference("foods");
            string foodQueryString = "";
            Boolean first = true;
            foreach (Store store in queryResultStores) {
                if (first == true) {
                    first = false;
                    foodQueryString = TableQuery.GenerateFilterCondition(
                        "storeName",
                        QueryComparisons.Equal,
                        store.RowKey
                    );
                } else {
                    foodQueryString = TableQuery.CombineFilters(
                        foodQueryString,
                        TableOperators.Or,
                        TableQuery.GenerateFilterCondition(
                            "storeName",
                            QueryComparisons.Equal,
                            store.RowKey
                        )
                    );
                }
            }
            TableQuery<FoodItem> foodsQuery = new TableQuery<FoodItem>().Where(foodQueryString);
            var queryResultFoods = foodsTable.ExecuteQuery(foodsQuery);

            // Execute query to find orders associated with food items in list.
            CloudTable ordersTable = tableClient.GetTableReference("orders");
            string orderQueryString = "";
            first = true;
            foreach (FoodItem foodItem in queryResultFoods) {
                if (first == true) {
                    first = false;
                    orderQueryString = TableQuery.GenerateFilterCondition(
                        "foodID",
                        QueryComparisons.Equal,
                        foodItem.RowKey
                    );
                } else {
                    orderQueryString = TableQuery.CombineFilters(
                        orderQueryString,
                        TableOperators.Or,
                        TableQuery.GenerateFilterCondition(
                            "foodID",
                            QueryComparisons.Equal,
                            foodItem.RowKey
                        )
                    );
                }
            }
            TableQuery<Order> orderQuery = new TableQuery<Order>().Where(orderQueryString);
            var queryResultOrders = ordersTable.ExecuteQuery(orderQuery);

            // Create list of foods within a given distance with no relevent order entity.
            OutputFoods outputObject = new OutputFoods();
            foreach (FoodItem foodItem in queryResultFoods) {
                // Find if food item has a relevent order.
                Boolean hasOrder = false;
                foreach (Order order in queryResultOrders) {
                    if (order.foodID == foodItem.RowKey) {
                        hasOrder = true;
                    }
                }

                // If there is no relevent order, then add the food item to a list to be returned.
                if (!hasOrder) {
                    outputObject.success = true;
                    outputObject.foods.Add(new FoodItemOutput(
                        foodItem.RowKey,
                        foodItem.name,
                        foodItem.storeName,
                        foodItem.category,
                        foodItem.quantity,
                        foodItem.unit
                    ));
                }
            }

            // Return list of food items.
            return new OkObjectResult(JsonConvert.SerializeObject(outputObject));
        }
    }
}
