using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;

namespace FoodBank.DataAccess
{
    public class Search : TableEntity {
        public string category {get; set;}
        public double latitude {get; set;}
        public double longitude {get; set;}
        public double distance {get; set;}
        public Search() {}
        public Search(string category, double latitude, double longitude, double distance) {
            // Generate searchID
            string searchID = string.Concat(DateTime.Now.GetHashCode(), category).GetHashCode().ToString();
            RowKey = searchID;
            PartitionKey = searchID;
            this.category = category;
            this.latitude = latitude;
            this.longitude = longitude;
            this.distance = distance;
        }
    }
    public static class addSearch
    {
        [FunctionName("addSearch")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to add a search to the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string category = data.category;
            double latitude = data.latitude;
            double longitude = data.longitude;
            double distance = data.distance;

             // Add search to Azure database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable searchesTable = tableClient.GetTableReference("searches");
            try {
                TableOperation insertOperation = TableOperation.Insert(new Search(category, latitude, longitude, distance));
                TableResult result = await searchesTable.ExecuteAsync(insertOperation);
            } catch (StorageException e) {
                return new OkObjectResult(string.Concat("{\"success\": false, \"message\":\"", e.ToString(), "\"}"));
            }
            
            return new OkObjectResult("{\"success\": true}");
        }
    }
}

