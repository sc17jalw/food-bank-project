import sys
import json, requests
import random
import time, datetime

this_module = sys.modules[__name__]
URL = 'https://dataaccesscomponentfoodbank.azurewebsites.net/api/'
functions = ['addSearch', 'getSearches', 'getCategories', 'getFoodItem', 'getAvailableFoodItemsDistance', 'getAvailableFoodItemsPostcode', 'getOrder', 'getOrders', 'getStoreDetails', 'getStoresDistance', 'getAvailableFoodsStore']
categories = json.loads(requests.get(URL + 'getCategories').text)['categories']
foods = ['-508185293']
orders = ['-228560151']
stores = ['Jake\'s Example Store']
latencies = []

def addSearch():
    category = random.choice(categories)
    latitude = random.uniform(-2.75, 0.2)
    longitude = random.uniform(51, 54)
    distance = random.uniform(1, 100)
    search = {
        'category': category,
        'latitude': latitude,
        'longitude': longitude,
        'distance': distance
    }
    print("Request: " + json.dumps(search))
    start_time = time.perf_counter()
    print("Result: " + requests.get(URL + "addSearch", data=json.dumps(search)).text)
    return time.perf_counter() - start_time

def getSearches():
    latitude = random.uniform(-2.75, 0.2)
    longitude = random.uniform(51, 54)
    distance = random.uniform(1, 100)
    query = {
        'latitude': latitude,
        'longitude': longitude,
        'distance': distance
    }
    print("Request: " + json.dumps(query))
    start_time = time.perf_counter()
    print("Result: " + requests.get(URL + "getSearches", data=json.dumps(query)).text)
    return time.perf_counter() - start_time


def getCategories():
    print("Request: empty")
    start_time = time.perf_counter()
    print("Result: " + requests.get(URL + 'getCategories').text)
    return time.perf_counter() - start_time


def getFoodItem():
    foodID = random.choice(foods)
    request = {
        'foodID': foodID
    }
    print("Request: " + json.dumps(request))
    start_time = time.perf_counter()
    print("Result: " + requests.get(URL + 'getFoodItem', data=json.dumps(request)).text)
    return time.perf_counter() - start_time


def getAvailableFoodItemsDistance():
    latitude = random.uniform(-2.75, 0.2)
    longitude = random.uniform(51, 54)
    distance = random.uniform(1, 100)
    request = {
        'latitude': longitude,
        'longitude': latitude,
        'distance': distance
    }
    print("Request: " + json.dumps(request))
    start_time = time.perf_counter()
    result = requests.get(URL + 'getAvailableFoodItemsDistance', data=json.dumps(request)).text
    end_time = time.perf_counter() - start_time
    print("Result: " + result)
    for item in json.loads(result)['foods']:
        if not item['foodID'] in foods:
            foods.append(item['foodID'])
    return end_time

def getAvailableFoodItemsPostcode():
    distance = random.uniform(1, 100)
    postcode = json.loads(requests.get("http://api.postcodes.io/random/postcodes").text)['result']['postcode']
    request = {
        'postcode': postcode,
        'distance': distance
    }
    print("Request: " + json.dumps(request))
    start_time = time.perf_counter()
    result = requests.get(URL + 'getAvailableFoodItemsPostcode', data=json.dumps(request)).text
    end_time = time.perf_counter() - start_time
    print("Result: " + result)
    for item in json.loads(result)['foods']:
        if not item['foodID'] in foods:
            foods.append(item['foodID'])
    return end_time

def getOrder():
    orderID = random.choice(orders)
    orderID = {
        'orderID': orderID
    }
    print("Request: " + json.dumps(orderID))
    start_time = time.perf_counter()
    print("Result: " + requests.get(URL + "getOrder", data=json.dumps(orderID)).text)
    return time.perf_counter() - start_time

def getOrders():
    # Generate a random range of dates
    end_date = datetime.datetime.now()
    start_date = end_date - datetime.timedelta(seconds=random.randint(0, 1123200))
    order_info = {
        'start_date': start_date.strftime("%d/%m/%Y %H:%M:%S"),
        'end_date': end_date.strftime("%d/%m/%Y %H:%M:%S")
    }
    print("Request: " + json.dumps(order_info))
    start_time = time.perf_counter()
    result = requests.get(URL + "getOrders", data=json.dumps(order_info)).text
    end_time = time.perf_counter() - start_time
    print("Result: " + result)
    for item in json.loads(result)['orders']:
        if not item['orderID'] in orders:
            orders.append(item['orderID'])
    return end_time

def getStoreDetails():
    store_name = random.choice(stores)
    request = {
        'name': store_name
    }
    print("Request: " + json.dumps(request))
    start_time = time.perf_counter()
    print("Result: " + requests.get(URL + "getStoreDetails", data=json.dumps(request)).text)
    return time.perf_counter() - start_time

def getStoresDistance():
    latitude = random.uniform(-2.75, 0.2)
    longitude = random.uniform(51, 54)
    distance = random.uniform(1, 100)
    query = {
        'latitude': latitude,
        'longitude': longitude,
        'distance': distance
    }
    print("Request: " + json.dumps(query))
    start_time = time.perf_counter()
    result = requests.get(URL + "getStoresDistance", data=json.dumps(query)).text
    end_time = time.perf_counter() - start_time
    print("Result: " + result)
    for item in json.loads(result)['stores']:
        if not item['name'] in stores:
            stores.append(item['name'])
    return end_time

def getAvailableFoodsStore():
    storeID = random.choice(stores)
    query = {
        'storeID': storeID
    }
    print("Request: " + json.dumps(query))
    start_time = time.perf_counter()
    result = requests.get(URL + "getAvailableFoodsStore", data=json.dumps(query)).text
    end_time = time.perf_counter() - start_time
    print("Result: " + result)
    for item in json.loads(result)['foods']:
        if not item['foodID'] in foods:
            foods.append(item['foodID'])
    return end_time


time_now = time.time()
while time.time() < time_now + 20:
    function_choice = random.choice(functions)
    print("Running function: " + function_choice + "...")
    function_to_call = getattr(this_module, function_choice)
    latency = function_to_call()
    latencies.append(latency)
    print("Latency: " + str(latency))
    print()

sum = 0
for item in latencies:
    sum += item
print("Average latency for function request: " + str(sum / len(latencies)))

