using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;

namespace FoodBank.DataAccess
{
    public class StoreOutput {
        public string name;
        public double latitude;
        public double longitude;
        public string information;
        public string address;
        public int distance;
        public StoreOutput(string name, double latitude, double longitude, string information, string address, int distance) {
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.information = information;
            this.address = address;
            this.distance = distance;
        }
    }

    public class OutputStores {
        public List<StoreOutput> stores = new List<StoreOutput>();
        public Boolean success = false;
    }
    public static class getStoresDistance
    {
        [FunctionName("getStoresDistance")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to retireve stores in given distance of a location from the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            double latitude = data.latitude;
            double longitude = data.longitude;
            double distance = data.distance;
            
            // Calculate target coordinates.
            Geolocation.CoordinateBoundaries boundaries = new Geolocation.CoordinateBoundaries(new Geolocation.Coordinate(latitude, longitude), distance);
            
            // Execute query.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable storesTable = tableClient.GetTableReference("stores");
            TableQuery<Store> storesDistanceQuery = new TableQuery<Store>().Where(
                TableQuery.CombineFilters(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("latitude", QueryComparisons.GreaterThanOrEqual, boundaries.MinLatitude),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("latitude", QueryComparisons.LessThanOrEqual, boundaries.MaxLatitude)
                    ),
                    TableOperators.And,
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDouble("longitude", QueryComparisons.GreaterThanOrEqual, boundaries.MinLongitude),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDouble("longitude", QueryComparisons.LessThanOrEqual, boundaries.MaxLongitude)
                    )
                )
            );
            var queryResult = storesTable.ExecuteQuery(storesDistanceQuery);

            // Format output.
            OutputStores outputObject = new OutputStores();
            foreach (Store store in queryResult) {
                outputObject.success = true;
                double distanceFromOrigin = Geolocation.GeoCalculator.GetDistance(latitude, longitude, store.latitude, store.longitude, 2);
                outputObject.stores.Add(new StoreOutput(store.RowKey, store.latitude, store.longitude, store.information, store.address, Convert.ToInt32(distanceFromOrigin)));
            }
            
            // Sort stores by distance.
            outputObject.stores.Sort((store1, store2) => store1.distance - store2.distance);

            return new OkObjectResult(JsonConvert.SerializeObject(outputObject));
        }
    }
}
