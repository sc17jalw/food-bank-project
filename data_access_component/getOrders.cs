using System.IO;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;

namespace FoodBank.DataAccess
{
    public class OrderOutput {
        public string orderID;
        public string foodID;
        public string userID;
        public string timestamp;
        public OrderOutput(string orderID, string foodID, string userID, string timestamp) {
            this.orderID = orderID;
            this.foodID = foodID;
            this.userID = userID;
            this.timestamp = timestamp;
        }
    }
    public class OutputOrders {
        public List<OrderOutput> orders = new List<OrderOutput>();
        public Boolean success = false;
    }
    public static class getOrders
    {
        [FunctionName("getOrders")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to retrieve orders between certain datetimes from the database.");

            // Get details from request body.
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string startDateString = data.start_date;
            DateTime startDate = new DateTime(  Int32.Parse(startDateString.Substring(6, 4)),
                                                Int32.Parse(startDateString.Substring(3, 2)),
                                                Int32.Parse(startDateString.Substring(0, 2)),
                                                Int32.Parse(startDateString.Substring(11, 2)),
                                                Int32.Parse(startDateString.Substring(14, 2)),
                                                Int32.Parse(startDateString.Substring(17, 2)));
            string endDateString = data.end_date;
            DateTime endDate = new DateTime(Int32.Parse(endDateString.Substring(6, 4)),
                                            Int32.Parse(endDateString.Substring(3, 2)),
                                            Int32.Parse(endDateString.Substring(0, 2)),
                                            Int32.Parse(endDateString.Substring(11, 2)),
                                            Int32.Parse(endDateString.Substring(14, 2)),
                                            Int32.Parse(endDateString.Substring(17, 2)));
            
            // Execute query.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable ordersTable = tableClient.GetTableReference("orders");
            TableQuery<Order> ordersDateQuery = new TableQuery<Order>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterConditionForDate("Timestamp", QueryComparisons.GreaterThanOrEqual, startDate),
                    TableOperators.And,
                    TableQuery.GenerateFilterConditionForDate("Timestamp", QueryComparisons.LessThanOrEqual, endDate)
                )
            );
            var queryResult = ordersTable.ExecuteQuery(ordersDateQuery);
            
            // Format output.
            OutputOrders outputObject = new OutputOrders();
            foreach (Order order in queryResult) {
                outputObject.success = true;
                outputObject.orders.Add(new OrderOutput(order.RowKey, order.foodID, order.userID, order.Timestamp.DateTime.ToString()));
            }
            

            return new OkObjectResult(JsonConvert.SerializeObject(outputObject));
        }
    }
}

