using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;

namespace FoodBank.DataAccess
{
    public class OutputCategories {
        public List<string> categories = new List<string>();
        public Boolean success = false;
    }
    public static class getCategories
    {
        [FunctionName("getCategories")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function to get available categories.");
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            // Get categories from database.
            CloudTableClient tableClient = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=datacomponentstorage;AccountKey=6Lwksk/tZKSUlLZwm3XK602wpjgwhzOhiCGgYFiISUfOZD8X5UlW8gDaEO6ItMNDNLTYu1Q4IsHuyzOR1YtAqQ==;EndpointSuffix=core.windows.net").CreateCloudTableClient(new TableClientConfiguration());
            CloudTable categoriesTable = tableClient.GetTableReference("categories");
            TableQuery<TableEntity> categoriesQuery = new TableQuery<TableEntity>();
            var queryResult = categoriesTable.ExecuteQuery(categoriesQuery);

            // Format output.
            OutputCategories outputObject = new OutputCategories();
            foreach (TableEntity entity in queryResult) {
                outputObject.success = true;
                outputObject.categories.Add(entity.RowKey);
            }
            
            return new OkObjectResult(JsonConvert.SerializeObject(outputObject));
        }
    }
}

