# FOOD DISTRIBUTION SITE

The food distribution site is used to search for and order food, and for businesses to list their food items.
Pipenv and Python 3.8 are required for setting up this part of the project.


### Setting up the pipenv
To run the site, you will first need to initialise a pipenv shell with the following command:
```bash
pipenv shell
```
This will install all dependencies for this part of the project.


### Running the site
The site can then be run locally on your machine using:
```bash
flask run
```
and visiting the URL given.


### Initialising the DB
The first time you run the site, you will need to initialise the accounts database.
This can be done by visiting the /dbcreate extension of the website's URL, or with the command:
```bash
python db_create.py
```
