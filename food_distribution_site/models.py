from food_distribution_site import db
from sqlalchemy.ext.associationproxy import association_proxy



class Account(db.Model):
    __tablename__ = 'Account'
    accountId = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), nullable=False, unique=True)
    password = db.Column(db.String(200), nullable=False)
    type = db.Column(db.String(200), nullable=False)
