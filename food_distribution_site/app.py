from flask import Flask, render_template, redirect, flash, url_for, Markup, session, request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FloatField, SelectField
from jinja2 import Template
#from .forms import loginForm,signupForm
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import Form
from wtforms import TextField, PasswordField, TextAreaField, BooleanField, RadioField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired
from sqlite3 import IntegrityError
from io import BytesIO
import hashlib
import requests
import json

import os
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_TRACK_MODIFICATIONS = True


app = Flask(__name__)
app.config['SECRET_KEY'] = 'ihu7i6giug8g90h08fiubi86f5d4dc9'
api = 'https://dataaccesscomponentfoodbank.azurewebsites.net/api/'
app.config['SQLALCHEMY_DATABASE_URI'] =  'sqlite:///' + os.path.join(basedir, 'app.db')
db = SQLAlchemy(app)

class loginForm(Form):
    username = TextField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

class signupForm(Form):
    username = TextField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    confirmPassword = PasswordField('confirmPassword', validators=[DataRequired()])
    type = RadioField('type', choices=[('user','Personal'),('shop','Business')])

class shopForm(Form):
    name = TextField('name', validators=[DataRequired()])
    info = TextField('info', validators=[DataRequired()])
    postcode = TextField('postcode', validators=[DataRequired()])

class Account(db.Model):
    __tablename__ = 'Account'
    accountId = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200), nullable=False, unique=True)
    password = db.Column(db.String(200), nullable=False)
    type = db.Column(db.String(200), nullable=False)

class Store(db.Model):
    __tablename__ = 'Store'
    storeId = db.Column(db.Integer, primary_key=True)
    linkedAccountID = db.Column(db.String(200), nullable=False, unique=True)
    name = db.Column(db.String(200), nullable=False)
    info = db.Column(db.String(200), nullable=False)
    postcode = db.Column(db.String(200), nullable=False)
    long = db.Column(db.String(200), nullable=False)
    lat = db.Column(db.String(200), nullable=False)


# FORMS

# Search
class SearchForm(FlaskForm):
    search = StringField('Search')
    distance = FloatField('Distance')
    category = SelectField('Category', choices=[('none', ' '), ('dairy', 'Dairy'), ('fruit', 'Fruit'), ('carbohydrates', 'Carbohydrate'), ('meat', 'Meat'), ('sweets', 'Sweet'), ('vegetables', 'Vegetables')])
    submit = SubmitField('Search')

# Order Button
class OrderForm(FlaskForm):
    order = SubmitField('Order Item')

# Add Food
class AddItemForm(FlaskForm):
    name = StringField('Item Name')
    category = SelectField('Category', choices=[('none', ' '), ('dairy', 'Dairy'), ('fruit', 'Fruit'), ('carbohydrates', 'Carbohydrate'), ('meat', 'Meat'), ('sweets', 'Sweet'), ('vegetables', 'Vegetables')])
    quantity = FloatField('Quantity')
    unit = StringField('Unit Name')
    submit = SubmitField('Add Item')




# NAVBAR
def getRNav():
    if 'sessionid' in session:
        if getAccountType(session['sessionid']) == "user":
            return "<li class=\"right\"><a href=\"/logout\">Sign Out</a></li> \n <li class=\"right\"><a href=\"/myorders\">My Orders</a></li>"
        else:
            return "<li class=\"right\"><a href=\"/logout\">Sign Out</a></li> \n <li class=\"right\"><a href=\"/additem\">Add Item</a></li>"

    else:
       return "<li class=\"right\"><a href=\"/Sign\">Sign In/Up</a></li>"




# PAGES

@app.route("/")
def home():
    rNav = getRNav()

    content = ""
    return render_template(
            'layout.html', pageContent=Markup(content), right_nav=Markup(rNav))



@app.route("/About")
def about():
    rNav = getRNav()
    content = ""
    content += " <img style=\"width: 60%; margin: 10% 20% 10%;\" src=\"../static/img/flowchart.png\">"
    try:
        print(session['sessionid'])
    except:
        print("No session")

    if 'sessionid' in session:
        print(getAccountType( session['sessionid'] ))

    return render_template(
            'layout.html', test=Markup(content), right_nav=Markup(rNav))




@app.route("/Categories")
def categories():
    rNav = getRNav()
    content = ""

    # Get categories
    data = json.loads(requests.get(api + 'getCategories').text)
    items = data['categories']

    # Display on page
    for cat in items:
        content += "<div class=\"category\">" +  "<a  href=\"/search/0/0/" + cat + "\" >" +cat + "</a> </div>"

    return render_template(
            'categories.html', categories=Markup(content), right_nav=Markup(rNav))




@app.route("/dbcreate", methods=['GET','POST'])
def dbcreate():
    flash("Attempting to create database...")
    try:
        db.create_all()
        flash("Done...")
    except:
        flash("Failed!")
    flash("Attempting to create Admin account...")
    try:
        account = Account(username="Admin",  password="leeds", type="Admin")
        flash("Done... Adding Admin account...")
        db.session.add(account)
        flash("Done... Commiting Admin account...")
        db.session.commit()
        flash("Done...")

    except:
        flash("Failed!")

    return render_template(
            'layout.html')



def addStore(LinkedAccountID, Name, Info, Postcode):
    try:
        print("Check1")
        PostcodeH = Postcode.replace(" ", "%20")
        print(Postcode)
        url = 'https://nominatim.openstreetmap.org/search?q='+PostcodeH+'&format=json'
        print(url)



        print("Check2")
        response = requests.get(url).json()
        print("Check3")
        print(response[0]["lat"])
        Lat = response[0]["lat"]
        print(response[0]["lon"])
        Long = response[0]["lon"]
        print("Check4")


        print("Done...")
        request = {
         'name': Name,
         'postcode': Postcode,
         'Address': "The Forbidden Shelf",
         'information': Info,
        }
        print(request)
        try:
            print("Uploading...")
            response = json.loads(requests.get(api + 'addStore',
                                        data=json.dumps(request)).text)
            print("Store added")
        except Exception as e:
            print(e)
            flash("Failed to sync store")
            return True

        account = Store(linkedAccountID=LinkedAccountID,  name=Name, info=Info, long=str(Long), lat=str(Lat), postcode=Postcode)
        print("Adding account...")
        db.session.add(account)
        print("Done... Commiting account...")
        db.session.commit()


        return True
    except Exception as e:
        print(e)
        flash("Postcode not found, remember to use spaces where appropriate.")
        return False

def addAccount(Username, Password, Type):
        account = Account(username=Username,  password=hashPassword(Password), type=Type)
        #flash("Adding account...")
        db.session.add(account)
        #flash("Done... Commiting account...")
        db.session.commit()
        #flash("Done...")




@app.route("/dbrecall", methods=['GET','POST'])
def dbrecall():
    flash("Recalling all accounts...")
    try:
        for accou in db.session.query(Account):

            print(str(accou.accountId)+", Username: "+str(accou.username)+", Password: "+str(accou.password)+", Account Type: "+str(accou.type))

        for stor in db.session.query(Store):

            print(str(stor.storeId)+", Owner: "+stor.linkedAccountID+", Name: "+stor.name+", Info: "+stor.info+", Lat: "+stor.lat+", long: "+stor.long)


        flash("Done")
    except Exception as e:
        print(e)
        flash("Failed!")

    return render_template(
            'layout.html')




@app.route("/search", methods=['GET','POST'])
def search():
    rNav = getRNav()
    content = ""

    # New search
    searchForm = SearchForm()
    if searchForm.submit.data:

        # Apply filters to URL or none value if field empty
        if searchForm.search.data != "":
            s = searchForm.search.data
        else:
         s = "0"

        if(searchForm.distance.data == None):
            d = 0
        else:
            d = searchForm.distance.data

        if(searchForm.category.data == ""):
            c = "none"
        else:
            c = searchForm.category.data

        return redirect(url_for('searchResultsFilter', searchString=s, dist=d, cat=c))


    return render_template(
            'search.html', form=searchForm, content=content, right_nav=Markup(rNav))




@app.route("/search/<string:searchString>/<int:dist>/<string:cat>", methods=['GET','POST'])
def searchResultsFilter(searchString, dist, cat):
    rNav = getRNav()
    content = ""

    # Make API request
    request = {
     'latitude': 1.5895,
     'longitude': 53.8123,
     'distance': dist
    }

    results = json.loads(requests.get(api + 'getAvailableFoodItemsDistance',
                                data=json.dumps(request)).text)

    # Construct results markup
    if searchString != "0":
        for i in results['foods']:
            if searchString.lower() in str(i['name']).lower():
                if str(i['category']) == str(cat) or cat == "none":
                    content += "<div class=\"itemShow\">"
                    content += "<a href=\"/item/" + i['foodID'] + "\" class=\"item\">"
                    content += " <img src=\"../../../static/img/"+str(i['category'])+".jpg\">"
                    content += " <p>" + i['name'] + " - " + str(i['quantity']) +" "+ i['unit'] + "</p>"
                    content += "</a> </div>"
                    #print(" <img src=\"../../../static/img/"+str(i['category'])+".jpg\">")
    else:
        for i in results['foods']:
            if str(i['category']) == str(cat) or cat == "none":
                content += "<div class=\"itemShow\">"
                content += "<a href=\"/item/" + i['foodID'] + "\" class=\"item\">"
                content += " <img src=\"../../../static/img/"+str(i['category'])+".jpg\">"
                content += " <p>" + i['name'] + " - " + str(i['quantity']) +" "+ i['unit'] + "</p>"
                content += "</a> </div>"
                #print(" <img src=\"../../../static/img/"+str(i['category'])+".jpg\">")



    # New search
    searchForm = SearchForm()
    if searchForm.submit.data:

        # Apply filters to URL or none value if field empty
        if searchForm.search.data != "":
            s = searchForm.search.data
        else:
         s = "0"

        if(searchForm.distance.data == None):
            d = 0
        else:
            d = searchForm.distance.data

        if(searchForm.category.data == ""):
            c = "none"
        else:
            c = searchForm.category.data

        return redirect(url_for('searchResultsFilter', searchString=s, dist=d, cat=c))



    return render_template(
            'search.html', form=searchForm, content=Markup(content), right_nav=Markup(rNav))




def loginCheckUser( username, password ):
    #flash("Log in checker working")
    password = hashPassword(password)

    for accou in db.session.query(Account):
        #flash("accounts")
        #flash(accou)
        #flash(accou.username)
        #flash(accou.password)
        #flash(accou.type)

        if accou.username == username and accou.password == password:
            return True


    for accou in db.session.query(Account):
        if accou.username == username and accou.password == password:
            return accou.accountId




def getIdForSession( username, password ):

    for accou in db.session.query(Account):
        print(accou)
        if accou.username == username and accou.password == hashPassword(password):
            print(accou)

            return accou.accountId

def getAccountType( id ):

    for accou in db.session.query(Account):
        if accou.accountId == id:
            return accou.type

def getLinkedStore( id ):
    for store in db.session.query(Store):
        print(store.linkedAccountID)
        if store.linkedAccountID == str(id):
            return store.name


@app.route("/Sign", methods=['GET', 'POST'])
def sign():
    rNav = getRNav()


    content = ""

    content += "Sign in or sign up<br>"

    form = loginForm()

    if form.validate_on_submit():
        try:
            #flash(form.username.data)
            #flash(form.password.data)
            if loginCheckUser(form.username.data,form.password.data) == True :
                accountID =  getIdForSession(form.username.data, form.password.data)
                print(accountID)
                session['sessionid'] = accountID
                flash("You have been logged in")
                try:
                    print(session['sessionid'])
                except:
                    print("No session")
                return redirect(url_for('about'))
        except:
            flash("Incorrect username or password has been provided. \nPlease try again.")

    return render_template(
            'login.html', pageContent=Markup(content),form=form, right_nav=Markup(rNav))





@app.route("/Signup", methods=['GET', 'POST'])
def signup():
    rNav = getRNav()
    content = ""

    content += "So you have chosen, sign up<br>"

    form = signupForm()
    if form.validate_on_submit():
        try:
            if form.password.data == form.confirmPassword.data:

                addAccount(form.username.data,form.password.data, form.type.data)

                # Take shop owners to the addStore page, sign them in
                if form.type.data == "shop":
                    session['sessionid'] = getIdForSession(form.username.data, form.password.data)
                    return redirect(url_for('createStore') )

                flash("Account Created, please sign in")
                return redirect(url_for('sign'))
            else:
                flash("Your passwords don't match, your account has not been created.")

        except Exception as e:
            print(e)
            flash("That username already exists")


    return render_template(
            'createAccount.html', pageContent=Markup(content),form=form, right_nav=Markup(rNav))

@app.route("/CreateStore", methods=['GET', 'POST'])
def createStore():
    rNav = getRNav()
#    if 'sessionid' not in session:
#        flash("You are not logged in, log in and try again")
#        return redirect(url_for('about'))
#    if getAccountType( session['sessionid'] ) != 'shop':
#        flash("Sorry, shop owners only")
#        return redirect(url_for('about'))

    form = shopForm()
    if form.validate_on_submit():

        if addStore(session['sessionid'],form.name.data,form.info.data,form.postcode.data):
            flash("Store Created")
            return redirect(url_for('about'))






    return render_template(
            'createShop.html',form=form, right_nav=Markup(rNav))


@app.route("/logout")
def logout():
    session.pop('sessionid',None)
    rNav = getRNav()
    flash("You have been logged out.")

    return render_template(
            'layout.html', pageContent="", right_nav=Markup(rNav))




@app.route('/item/<foodIDstr>', methods=['GET', 'POST'])
def viewItem(foodIDstr):
    rNav = getRNav()
    content = ""

    orderForm = OrderForm()

    # Get item info
    request = {
     'foodID': int(foodIDstr),
    }

    item = json.loads(requests.get(api + 'getFoodItem',
                                data=json.dumps(request)).text)

    name = item['name']
    quantity = str(item['quantity']) + " " + item['unit']
    store = item['store_name']
    img = "<img src=\"../../../static/img/"+str(item['category'])+".jpg\" style=\"width: 40%; float: left;\">"

    # Get store info to display on page
    request = {
      'name': item['store_name']
    }
    storeJSON = json.loads(requests.get(api + 'getStoreDetails',
                                data=json.dumps(request)).text)

    storeInfo = storeJSON['information']
    storeAdd = storeJSON['address']



    # Order button
    if orderForm.validate_on_submit():
        request = {
         'foodID': foodIDstr,
         'userID': session['sessionid']
        }

        response = json.loads(requests.get(api + 'addOrder',
                                    data=json.dumps(request)).text)

        flash("Your order has been placed. please collect the item in store using your digital receipt as proof.")
        return redirect(url_for('about'))

    # Hide order button unless signed in as user
    if 'sessionid' in session and getAccountType(session['sessionid']) == "user":
        hide = ""
    else:
        hide = "display: none;"


    return render_template(
            'item.html', hide=hide, form=orderForm, itemName=name, distance=quantity, store=store, info=storeInfo, add=storeAdd, right_nav=Markup(rNav), img=Markup(img))



@app.route('/myorders')
def myOrders():
    rNav = getRNav()
    content = ""

    content += "<div class=\"showOrder\"><h2> " + "Select an order:" + " </h2></div>"


    # Request orders for user
    request = {
     'userID': str(session['sessionid'])
    }

    orders = json.loads(requests.get(api + 'getOrdersUser',
                                data=json.dumps(request)).text)


    dates = []
    # Get list of [date, store]
    for i in orders['orders']:
        request = {
         'foodID': int(i['foodID']),
        }

        # Get info of specific item ordered
        item = json.loads(requests.get(api + 'getFoodItem',
                                    data=json.dumps(request)).text)

        time = (i['timestamp'].split(" "))[0]
        # Add store/date to list
        if [time, item['store_name']] not in dates:
            dates.append([time, item['store_name']])
            content += "<a style=\"text-decoration: none;\" href=\"order/" + time.replace('/', '-') + "/" + item['store_name'].replace(' ', '_') + "\">"
            content += "<div class=\"showOrderDate\"><b>"

            content +=  time + " - "
            content +=  item['store_name']

            content += "</b>"
            content +=  "</div></a>"


    return render_template(
            'viewOrders.html',  orders=Markup(content), right_nav=Markup(rNav))




@app.route('/order/<date>/<store>')
def viewOrder(date, store):
    rNav = getRNav()
    content = ""
    date = date.replace('-', '/')
    store = store.replace('_', ' ')

    content += "<div class=\"showOrder\"><h2> Orders from " + store + " on " + date + " </h2></div>"

    # Request orders for user
    request = {
     'userID': str(session['sessionid'])
    }

    orders = json.loads(requests.get(api + 'getOrdersUser',
                                data=json.dumps(request)).text)

    # Get orders for date/store
    for i in orders['orders']:
        request = {
         'foodID': int(i['foodID']),
        }

        # Get info of specific item ordered
        item = json.loads(requests.get(api + 'getFoodItem',
                                    data=json.dumps(request)).text)

        time = (i['timestamp'].split(" "))[0]
        if item['store_name'] == store and date == time:
            content += "<div class=\"showOrder\">"
            content +=  item['name'] + "  (" + str(item['quantity']) + " " + item['unit'] + ")"
            content +=  "</div>"


    return render_template(
            'viewOrders.html',  orders=Markup(content), right_nav=Markup(rNav))


def hashPassword(password):
    salt = b'MuchSecure'
    bytePassword = password.encode('utf-8')
    hashedPassword = hashlib.pbkdf2_hmac('sha256', bytePassword, salt, 100000)
    return hashedPassword

@app.route('/additem', methods=['GET', 'POST'])
def addItem():
    rNav = getRNav()
    content = ""

    form = AddItemForm()
    storeName = getLinkedStore( session['sessionid'] )

    # Add item to API
    if form.validate_on_submit():
        request = {
         'name': form.name.data,
         'store_name': storeName,
         'category': form.category.data,
         'quantity': form.quantity.data,
         'unit': form.unit.data
        }

        response = json.loads(requests.get(api + 'addFoodItem',
                                    data=json.dumps(request)).text)

        flash("Your item has been listed.")
        return redirect(url_for('about'))

    return render_template(
            'addItem.html',  form=form, right_nav=Markup(rNav))
