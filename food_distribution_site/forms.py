from flask_wtf import Form
from wtforms import TextField, PasswordField, TextAreaField, BooleanField, RadioField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired

class loginForm(Form):
    username = TextField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

class signupForm(Form):
    username = TextField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    confirmPassword = PasswordField('confirmPassword', validators=[DataRequired()])
