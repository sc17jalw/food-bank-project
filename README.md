# Food Bank Project

This repository contains the source code for each component that makes up a system that it used to distribute and monitor the demand for surplus food from small shops

## Data Access Component
The data access component is used by the two other websites to interact with the data of the system.

The component has been deployed using Azure and the API is available at the following URL:
[https://dataaccesscomponentfoodbank.azurewebsites.net/api/](https://dataaccesscomponentfoodbank.azurewebsites.net/api/)

See the code behind the running component in the following directory:
```bash
data_access_component
```

## Food Distribution Site
The food distribution site is used by shop owners and people in need to advertise and find unsold food.

Enter the following directory to see how to deploy this website:
```bash
food_distribution_site
```

## Data Analytics Site
The data analytics component is used by organizations to view data analytics pertaining to food stores and sales.

Enter the following directory to see how to deploy this website:
```bash
data_analytics_site
```
