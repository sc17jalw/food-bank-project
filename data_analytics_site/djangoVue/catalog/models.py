from django.db import models

from django.db import models
from django import forms
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


# Create your models here.
# TODO: Swap out "Organizations" for users
class Organizations(models.Model):
    '''
    Users of the service (organizations) can view data analytics information.
    '''

    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=500)
    email = models.EmailField(max_length=50)
    postcode = models.CharField(max_length=10, default="")
    longitude = models.FloatField(default=0)
    latitude = models.FloatField(default=0)
    token = models.CharField(max_length=500, unique=True, default="")

    def __str__(self):
        return str(self.username + ", " + self.email)



# TODO: Create new class Organizations
# TODO: Many users to 1 organization relationship for organisations

'''
# CLASS ORGANISATION
    -  NAME
    -  POSTCODE / Geolocation
'''