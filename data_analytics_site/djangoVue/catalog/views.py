from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import user_passes_test
from functools import wraps
import os, requests
from datetime import datetime, date
from passlib.hash import pbkdf2_sha256
import time, calendar, uuid


## Import registered models
from .models import Organizations

import re, json

debugMode = 0
api = 'https://dataaccesscomponentfoodbank.azurewebsites.net/api/'


# def loginRequired(function):
#     ''' This validates whether or not a user is logged in. '''
#     def wrap(request, *args, **kwargs):
        
#         if request.session.get("loggedIn", False):
#             return function(request, *args, **kwargs)

#         return redirect("redirect/")


#     wrap.__doc__ = function.__doc__
#     wrap.__name__ = function.__name__
#     return wrap



##==================================
##
##  VALIDATION CHECK FUNCTIONS
##
##==================================
longLat = [0,0]
def invalidUsername(username): # Ensure the username entered meets username standards

    if Organizations.objects.filter(username__exact=username): # check if username is taken
        return "The username you have entered is taken"

    if not len(username) or len(username)>=30: # check username length
        return "Username must be between 1 and 30 characters in length."

    if " " in username: # check username for illegal characters
        return "Usernames may not contain spaces."

    return ""

def invalidToken(token): # Ensure the session token is unique

    if Organizations.objects.filter(token__exact=token): # check if token is taken
        return True # Token is taken
    else:
        return False # Token is not taken


def invalidPassword(password): # Ensure the password the user entered meets password standards

    if (len(password)<6 or len(password)>=30): # check password length   
        return "Passwords must be between 6 and 30 characters in length."

    return ""

def invalidEmail(email):
    # Ensure the email entered is valid

    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if not re.search(regex,email):
        return "Invalid email format."

    return ""

def invalidPostcode(postcode):
    global longLat
    # Get postcode coordinates

    s = requests.Session()  # Create session in which all requests will take place

    r = s.get('https://api.postcodes.io/postcodes/'+postcode)
    parsed_json = json.loads(r.text)


    if parsed_json['status'] == 404:
        return parsed_json['error']


    longLat[0] = parsed_json['result']['longitude']
    longLat[1] = parsed_json['result']['latitude']

    return ""

def fetchUserData(token):
    ## Fetch User
    sc=Organizations.objects.filter(token__exact=token)
    if sc.count() > 0:  ## Check if user exists first
        return Organizations.objects.get(token__exact=token)
    else:
        return None






##==================================
##
##  LOGIN & REGISTRATION FUNCTIONS 
##
##==================================
@csrf_exempt
def apiRegister(request): # POST
    ''' This is used to allow a user to register to the service using a username, email and a password.
    When the command is invoked, the program prompts the user to enter the username, email,
    and password of the new user.'''

    try:
        username = request.POST["username"]
        password = request.POST["password"]
        postcode = request.POST["postcode"]
        email    = request.POST["email"]

        check = []
        check.append(invalidUsername(username))
        check.append(invalidPassword(password))
        check.append(invalidEmail(email))
        check.append(invalidPostcode(postcode))


        if (check.count(check[0]) == len(check)): # If there are no errors

            # hash the user password
            hashedPass = pbkdf2_sha256.encrypt(password, rounds=12000, salt_size=32)

            # Create user
            p1 = Organizations.objects.create(
                username = username,
                password = hashedPass,
                email = email,
                longitude = longLat[0],
                latitude = longLat[1],
                postcode = postcode)

            p1.save()

            jsonData = '''
            {
                "success": true
            }
            '''
            return JsonResponse(json.loads(jsonData), safe=False)


        # Concatenate error responses
        response = []
        for item in check:
            if item!="":
                response.append(item)

        jsonData = '''
        {
            "success": false,
            "response": '''+"\n".join([str(p) for p in response])+'''
        }
        '''

        return JsonResponse(json.loads(jsonData), safe=False)
            

    except Exception as e:
        if debugMode:
            raise e
        else:
            print(e)

        jsonData = '''
            {
                "success": false,
                "response": "Invalid Request"
            }
            '''
        return JsonResponse(json.loads(jsonData), safe=False)




@csrf_exempt
def apiLogin(request): # POST
    ''' This command is used to log in to the service.'''

    try:
        username = request.POST["username"]
        password = request.POST["password"]

        u = None

        ## Fetch User
        sc=Organizations.objects.filter(username__exact=username)
        if sc.count() > 0:  ## Check if user exists first
            u = Organizations.objects.get(username__exact=username)

        ## Verify password
        if (u!= None and pbkdf2_sha256.verify(password,u.password)):
            request.session['loggedIn'] = True
            request.session['username'] = username
            
            ## Create session token
            tokenIsInvalid = True
            loginToken = ""
            while tokenIsInvalid: ## loop until valid (unique) token is created
                loginToken = str(uuid.uuid4())
                # print ("Checking token "+ loginToken)
                tokenIsInvalid = invalidToken(loginToken)

            u = Organizations.objects.get(username__exact=username)
            u.token = loginToken
            u.save()

            ## CREATE JSON
            jsonData = '''
            {
                "success": true,
                "token": "'''+ loginToken +'''"
            }
            '''
            return JsonResponse(json.loads(jsonData), safe=False)

        else:

            ## CREATE JSON
            jsonData = '''
                {   "success": false,
                    "response": "The username or password is incorrect."}
            '''
            return JsonResponse(json.loads(jsonData), safe=False)


    except Exception as e:
        if debugMode:
            raise e
        else:
            print(e)

        jsonData = '''
            {
                "success": false,
                "response": "Invalid Request"
            }
            '''
        return JsonResponse(json.loads(jsonData), safe=False)



@csrf_exempt
def apiUpdateAccountInfo(request):
    
    try:
        username = request.POST["username"]
        password = request.POST["password"]
        postcode = request.POST["postcode"]
        email    = request.POST["email"]
        token    = request.POST["token"]

        ## Fetch user by token
        u = fetchUserData(token)

        jsonData = ""
        if u!=None:

            check = []
            ## If user wants to change their Username
            if (oldUsername!=username):
                check.append(invalidUsername(username))

            check.append(invalidPassword(password))
            check.append(invalidEmail(email))
            check.append(invalidPostcode(postcode))


            if (check.count(check[0]) == len(check)): # If there are no errors

                # hash the user password
                hashedPass = pbkdf2_sha256.encrypt(password, rounds=12000, salt_size=32)

                # Update user
                u = Organizations.objects.get(username__exact=username)
                u.username  = username
                u.password  = hashedPass
                u.email     = email
                u.longitude = longLat[0]
                u.latitude  = longLat[1]
                u.postcode  = postcode

                u.save()

                p1 = Organizations.objects.create(
                    username = username,
                    password = hashedPass,
                    email = email,
                    longitude = longLat[0],
                    latitude = longLat[1],
                    postcode = postcode)

                p1.save()

                jsonData = '''
                {
                    "success": true
                }
                '''
                return JsonResponse(json.loads(jsonData), safe=False)

            # Concatenate error responses
            response = []
            for item in check:
                if item!="":
                    response.append(item)

            jsonData = '''
                {
                    "success": false,
                    "response": "'''+"\n".join([str(p) for p in response])+'''",
                }
                '''

        else:
            # Token not found so the session is invalidated
            jsonData = '''
                {
                    "success": false,
                    "response": "Invalid Session",
                }
                '''
                
        return JsonResponse(json.loads(jsonData), safe=False)
    

    except Exception as e:
        if debugMode:
            raise e
        else:
            print(e)

        jsonData = '''
            {
                "success": false,
                "response": "Invalid Request"
            }
            '''
        return JsonResponse(json.loads(jsonData), safe=False)


@csrf_exempt
def apiGetUserAccountInfo(request):     # "api/getUserInfo"

    try:
        token = request.POST["token"]

        u = fetchUserData(token)

        jsonData = ""
        if u!=None:
            token = request.POST["token"]
            u = Organizations.objects.get(username__exact=username)

            jsonData = '''
            {
                "success": true,
                "username": "'''+ u.username +'''",
                "postcode": "'''+ u.postcode +'''",
                "email": "'''+ u.email +'''",
                "longitude": '''+ str(u.longitude) +''',
                "latitude": '''+ str(u.latitude) +'''
            }
            '''

        else:
            # Token not found so the session is invalidated
            jsonData = '''
                {
                    "success": false,
                    "response": "Invalid Session",
                }
                '''
                
        return JsonResponse(json.loads(jsonData), safe=False)
    
    except Exception as e:
        if debugMode:
            raise e
        else:
            print(e)

        jsonData = '''
            {
                "success": false,
                "response": "Invalid Request"
            }
            '''
        return JsonResponse(json.loads(jsonData), safe=False)

@csrf_exempt
def apiLogout(request):     # "api/logout"
    ''' This causes the user to logout from the current session. '''

    try:
        # Get a list of session variables
        keyList = []
        for key in request.session.keys():
            keyList.append(key)

        # Delete session variables
        for key in keyList:
            del request.session[key]

        jsonData = '''
            {
                "success": true,
                "response": "Logout successful."
            }
            '''
        return JsonResponse(json.loads(jsonData), safe=False)

    except Exception as e:
        if debugMode:
            raise e
        else:
            print(e)

        jsonData = '''
            {
                "success": false,
                "response": "Invalid Request"
            }
            '''
        return JsonResponse(json.loads(jsonData), safe=False)

@csrf_exempt
def apiRedirect(request):
    return HttpResponse("You must be logged in to perform that action.")






##============================================
##
##  DATA ANALYTICS / VISUALIZATION FUNCTIONS
##
##============================================
@csrf_exempt
def apiShortestRoute(request):  # "api/shortestRoute"
    
    try:
        token = request.POST["token"]

        u = fetchUserData(token)

        jsonData = ""
        if u!=None:

            ## Get closest store
            distance = 1000
            payload = {
                'latitude': str(u.latitude),
                'longitude': str(u.longitude),
                'distance': str(distance),
                'postcode': str(u.postcode)
            }
            s = requests.Session()  # Create session in which all requests will take place

            r = s.get(api + 'getStoresDistance', data=json.dumps(payload))
            # print(json.dumps(r.json(), indent=4, sort_keys=True))


            if (r.json()['success'] == True):
                ## Create Json
                jsonData = '''
                {
                    "success": true,
                    "originLongitude": '''+ str(u.longitude) +''',
                    "originLatitude": '''+ str(u.latitude) +''',
                    "destinationLongitude": '''+ str(r.json()['stores'][0]['longitude']) +''',
                    "destinationLatitude": '''+ str(r.json()['stores'][0]['latitude']) +'''
                }
                '''
            else:
                # Token not found so the session is invalidated
                jsonData = '''
                {
                    "success": false,
                    "response": "No stores within 1000km of your location"
                }
                '''
        else:
            # Token not found so the session is invalidated
            jsonData = '''
                {
                    "success": false,
                    "response": "Invalid Session"
                }
                '''

        return JsonResponse(json.loads(jsonData), safe=False)

    except Exception as e:
        if debugMode:
            raise e
        else:
            print(e)

        jsonData = '''
        {
            "success": false,
            "response": "Invalid Request"
        }
        '''
        return JsonResponse(json.loads(jsonData), safe=False)



minSearches = -1 
maxSearches = 0
regionData = [
    ["EA", 52.319445, 0.569379, 90,  "EA  East                    ", None, None, "01"],
    ["EM", 53.045354,-0.708151, 80,  "EM  East Midlands           ", None, None, "02"],
    ["LO", 51.509276,-0.120977, 30,  "LO  London                  ", None, None, "03"],
    ["NE", 55.009398,-1.669301, 70,  "NE  North East              ", None, None, "04"],
    ["NW", 54.127696,-3.180320, 90,  "NW  North West              ", None, None, "05"],
    ["SE", 50.821843,-0.141969, 90,  "SE  South East              ", None, None, "06"],
    ["SW", 50.706414,-3.518579, 125, "SW  South West              ", None, None, "07"],
    ["WM", 52.395670,-2.433784, 100, "WM  West Midlands           ", None, None, "08"],
    ["YH", 54.025048,-1.086765, 90,  "YH  Yorkshire and the Humber", None, None, "09"]
]
@csrf_exempt
def apiDemandHeatmap(request):          # "api/demandHeatmap"
    global minSearches, maxSearches
    minSearches = -1 
    maxSearches = 0
    
    def GetHeatmapRegionData(longitude,latitude,distance):
        global minSearches, maxSearches

        payload = {
            'latitude': latitude,
            'longitude': longitude,
            'distance': distance
        }
        s = requests.Session()  # Create session in which all requests will take place

        r = s.get(api + 'getSearches', data=json.dumps(payload))
        parsed_json = json.loads(r.text)

        # print(json.dumps(parsed_json, indent=4, sort_keys=True))

        # quantityTotal = 0
        # for item in parsed_json['foods']:
        #     quantityTotal += item['quantity']

        quantityTotal = 0
        for item in parsed_json['searches']:
            quantityTotal += 1


        if (quantityTotal<minSearches or minSearches==-1):
            minSearches = quantityTotal 

        if (quantityTotal>maxSearches):
            maxSearches = quantityTotal 

        return quantityTotal


    # Caption data
    heatmap = '''
    {
        "chart": {
            "caption": "Food Levels",
            "subcaption": " Between Areas",
            "numbersuffix": "",
            "includevalueinlabels": "1",
            "labelsepchar": ": ",
            "entityFillHoverColor": "#FFF9C4",
            "theme": "fusion"
        },
    '''

    # Get quantity data via api
    heatmapData = '''"data": ['''
    for region in regionData:

        heatmapData+="{"
        heatmapData+='''"id": "'''+region[7]+'''" ,'''
        # print("\n")
        print("GetHeatmapRegionData for "+str(region[4]))
        value = GetHeatmapRegionData(region[1],region[2],region[3])
        heatmapData+='''"value": "'''+str(value)+'''",'''
        heatmapData+='''"showLabel": "1"'''

        heatmapData+="}"

        # If not at the last element, then add a comma
        if (region != regionData[-1]):
            heatmapData+=","


    heatmapData += ''' ] }'''

    print(maxSearches)
    # Range data
    heatmapColorRange = '''
        "colorrange": {
            "minvalue": "0",
            "code": "#FFE0B2",
            "gradient": "1",
            "color": [

                {
                    "color": "#FFD74D",
                    "minvalue": "0.5",
                    "maxvalue": "''' + str(round(maxSearches*(1/3))) + '''.0"
                },
                {
                    "color": "#FB8C00",
                    "minvalue": "''' + str(round(maxSearches*(1/3))) + '''.0",
                    "maxvalue": "''' + str(round(maxSearches*(2/3))) + '''.0"
                },
                {
                    "color": "#E65100",
                    "minvalue": "''' + str(round(maxSearches*(2/3))) + '''.0",
                    "maxvalue": "''' + str(maxSearches) + '''.0"
                }
            ]
        },
    '''

    # Combine data
    heatmap = heatmap + heatmapColorRange + heatmapData

    # RETURN heatmap.json
    return JsonResponse(json.loads(heatmap), safe=False)



@csrf_exempt
def apiAreaDemandLevel(request):    # "api/areaDemandLevel"

    def GetRegionData(longitude,latitude,distance,index):
        global minSearches, maxSearches

        ## Make search request in location
        payload = {
            'latitude': latitude,
            'longitude': longitude,
            'distance': distance
        }
        s = requests.Session()  # Create session in which all requests will take place

        ## Do request
        r = s.get(api + 'getSearches', data=json.dumps(payload))
        parsed_json = json.loads(r.text)


        ## Calculate number of given searches
        searchCompile = [[],[]]  # category / count
        quantityTotal = 0
        for item in parsed_json['searches']:
            quantityTotal += 1

            ## Check if category has been encountered before
            if item['category'] in searchCompile[0]:
                ## add one search to category
                searchCompile[1][searchCompile[0].index(item['category'])] += 1
            else:
                ## Add new category to list
                searchCompile[0].append(item['category'])
                searchCompile[1].append(1)

        # round off values and set for suffix k (thousand)
        regionData[index][5] = str(round(quantityTotal*0.001, 3)) # Set number of searches


        ## Compile search result data
        categoryIndex = -1
        regionData[index][6] = ""
        for category in searchCompile[0]:
            categoryIndex+=1

            # round off values and set for suffix k (thousand)
            searchValue = str(round(searchCompile[1][categoryIndex]*0.001, 3))

            regionData[index][6] += '''{
                "label": "'''+category.title()+'''",
                "value": "'''+searchValue+'''",
                "displayValue": "'''+searchValue+'''K"
            }
            '''

            # If not at the last element, then add a comma
            if (category != searchCompile[0][-1]):
                regionData[index][6]+=","


        ## Find maximum and minimum number of searches
        if (quantityTotal<minSearches or minSearches==-1):
            minSearches = quantityTotal 

        if (quantityTotal>maxSearches):
            maxSearches = quantityTotal 

        return quantityTotal


    index = -1;
    for region in regionData:
        index+=1;
        print("GetRegionData for "+str(region[4]))

        value = GetRegionData(region[1],region[2],region[3],index)


    ## CREATE JSON
    barchartJson = '''
    {
    "chart": {
        "caption": "Search demand by Region",
        "subcaption": "Click to see individual information on Food Categories",
        "xaxisname": "Region",
        "yaxisname": "Searches (Thousand)",
        "numbersuffix": "K",
        "theme": "fusion",
        "rotateValues": "0"
    },
    "data": [
        {
            "label": "East",
            "value": "'''+regionData[0][5]+'''",
            "link": "newchart-xml-East"
        },
        {
            "label": "East Midlands",
            "value": "'''+regionData[1][5]+'''",
            "link": "newchart-xml-EastMidlands"
        },
        {
            "label": "London",
            "value": "'''+regionData[2][5]+'''",
            "link": "newchart-xml-London"
        },
        {
            "label": "North East",
            "value": "'''+regionData[3][5]+'''",
            "link": "newchart-xml-NorthEast"
        },
        {
            "label": "North West",
            "value": "'''+regionData[4][5]+'''",
            "link": "newchart-xml-NorthWest"
        },
        {
            "label": "South East",
            "value": "'''+regionData[5][5]+'''",
            "link": "newchart-xml-SouthEast"
        },
        {
            "label": "South West",
            "value": "'''+regionData[6][5]+'''",
            "link": "newchart-xml-SouthWest"
        },
        {
            "label": "West Midlands",
            "value": "'''+regionData[7][5]+'''",
            "link": "newchart-xml-WestMidlands"
        },
        {
            "label": "Yorkshire and the Humber",
            "value": "'''+regionData[8][5]+'''",
            "link": "newchart-xml-Yorkshire"
        }
    ],
        "linkeddata": [
            {
                "id": "East",
                "linkedchart": {
                "chart": {
                    "caption": "East - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[0][6]+''']
                }
            },
            {
                "id": "EastMidlands",
                "linkedchart": {
                "chart": {
                    "caption": "East Midlands - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[1][6]+''']
                }
            },
            {
                "id": "London",
                "linkedchart": {
                "chart": {
                    "caption": "London - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[2][6]+''']
                }
            },
            {
                "id": "NorthEast",
                "linkedchart": {
                "chart": {
                    "caption": "North East - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[3][6]+''']
                }
            },
            {
                "id": "NorthWest",
                "linkedchart": {
                "chart": {
                    "caption": "North West - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[4][6]+''']
                }
            },
            {
                "id": "SouthEast",
                "linkedchart": {
                "chart": {
                    "caption": "South East - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[5][6]+''']
                }
            },
            {
                "id": "SouthWest",
                "linkedchart": {
                "chart": {
                    "caption": "South West - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[6][6]+''']
                }
            },
            {
                "id": "WestMidlands",
                "linkedchart": {
                "chart": {
                    "caption": "West Midlands - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[7][6]+''']
                }
            },
            {
                "id": "Yorkshire",
                "linkedchart": {
                "chart": {
                    "caption": "Yorkshire and the Humber - By Food Type",
                    "subcaption": "Searches (Thousands)",
                    "numbersuffix": "K",
                    "theme": "fusion",
                    "rotateValues": "0",
                    "plottooltext": "$label, $dataValue,  $percentValue"
                },
                "data": ['''+regionData[8][6]+''']
                }
            }
            
        ]
    }'''


    # RETURN barchart.json
    return JsonResponse(json.loads(barchartJson), safe=False)


@csrf_exempt
def apiInventoryLevel(request):
    return HttpResponse("PLACEHOLDER")


@csrf_exempt
def apiDataTrend(request):      # "api/dataTrend"


    def getOrderData(dayIncrement):
        print("Getting order data " + str(dayIncrement))
        dayInSeconds = 86400
        ts = int(time.time()) - (dayIncrement*dayInSeconds) # Get current unix time
        currentDate = datetime.utcfromtimestamp(ts).strftime('%d/%m/%Y')


        ## Make search request in location  
        payload = {
            'start_date': currentDate+' 00:00:00',
            'end_date': currentDate+' 23:59:59'
        }
        s = requests.Session()  # Create session in which all requests will take place

        ## Do request
        r = s.get(api + 'getOrders', data=json.dumps(payload))
        parsed_json = json.loads(r.text)
        # print(json.dumps(parsed_json, indent=4, sort_keys=True))


        ## Calculate number of orders
        orderCount = 0
        for item in parsed_json['orders']:
            orderCount += 1

        ## Return total number of orders in time
        dayName = calendar.day_name[datetime.utcfromtimestamp(ts).weekday()]

        return [dayName, orderCount]

    ## COLLECT ORDER DATA
    orderList = []
    orderList.append(getOrderData(0))
    orderList.append(getOrderData(1))
    orderList.append(getOrderData(2))
    orderList.append(getOrderData(3))
    orderList.append(getOrderData(4))
    orderList.append(getOrderData(5))
    orderList.append(getOrderData(6))

    ## GET DAY TODAY
    dayTodayName = calendar.day_name[(date.today()).weekday()]

    ## Get current time
    currentTime = datetime.utcfromtimestamp(time.time()).strftime('%H:%M') # Time format 

    trendsJson = '''{
    "chart": {
        "caption": "Overall Orders per Day of the Week",
        "subCaption": "Last week",
        "xAxisName": "Day",
        "yAxisName": "No. of Orders",
        "showValues": "0",
        "theme": "fusion"
    },
    "annotations": {
        "groups": [
            {
                "id": "anchor-highlight",
                "items": [
                    {
                        "id": "high-star",
                        "type": "circle",
                        "x": "$dataset.0.set.6.x",
                        "y": "$dataset.0.set.6.y",
                        "radius": "12",
                        "color": "#6baa01",
                        "border": "2",
                        "borderColor": "#f8bd19"
                    },
                    {
                        "id": "label",
                        "type": "text",
                        "text": "'''+currentTime+''' - '''+dayTodayName+'''",
                        "fillcolor": "#6baa01",
                        "rotate": "90",
                        "x": "$dataset.0.set.6.x+75",
                        "y": "$dataset.0.set.6.y-2"
                    }
                ]
            }
        ]
    },
        "data": [
            {
                "label": "'''+str(orderList[6][0])+'''",
                "value": "'''+str(orderList[6][1])+'''"
            },
            {
                "label": "'''+str(orderList[5][0])+'''",
                "value": "'''+str(orderList[5][1])+'''"
            },
            {
                "label": "'''+str(orderList[4][0])+'''",
                "value": "'''+str(orderList[4][1])+'''"
            },
            {
                "label": "'''+str(orderList[3][0])+'''",
                "value": "'''+str(orderList[3][1])+'''"
            },
            {
                "label": "'''+str(orderList[2][0])+'''",
                "value": "'''+str(orderList[2][1])+'''"
            },
            {
                "label": "'''+str(orderList[1][0])+'''",
                "value": "'''+str(orderList[1][1])+'''"
            },
            {
                "label": "'''+str(orderList[0][0])+'''",
                "value": "'''+str(orderList[0][1])+'''"
            }
        ]
    }'''


    # RETURN trends.json
    return JsonResponse(json.loads(trendsJson), safe=False)


