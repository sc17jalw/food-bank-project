import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Main from '@/components/Main'
import LevelHeatmap from '@/components/LevelHeatmap'
import BarChart from '@/components/BarChart'
import Trends from '@/components/Trends'
import Register from '@/components/Register'
import Redirect from '@/components/Redirect'
import Route from '@/components/Route'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/redirect',
      name: 'Redirect',
      component: Redirect
    },
    {
      path: '/register',
      name: 'Resgister',
      component: Register
    },
    {
      path: '/main',
      name: 'Main',
      component: Main
    },
    {
      path: '/heatmap',
      name: 'LevelHeatmap',
      component: LevelHeatmap
    },
    {
      path: '/barchart',
      name: 'BarChart',
      component: BarChart
    },
    {
      path: '/trends',
      name: 'Trends',
      component: Trends
    },
    {
      path: '/route',
      name: 'Route',
      component: Route
    }
  ]
})
